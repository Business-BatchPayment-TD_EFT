#!perl -T

use Test::More tests => 1;

BEGIN {
    use_ok( 'Business::BatchPayment::TD_EFT' ) || print "Bail out!\n";
}

diag( "Testing Business::BatchPayment::TD_EFT $Business::BatchPayment::TD_EFT::VERSION, Perl $], $^X" );
